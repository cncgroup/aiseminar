<?php
//GET::
$get_admin = "admin";
$get_logout = "logout";
$get_upolad = "upload";

//FILES::
$script_login = "login.php";
$script_admin = "admin.php";
$script_upload = "upload.php";
$script_main = "main.phtml";
$script_navi = "navi.php";

//DB::
/*
$database_host = "kikaludo.ii.fmph.uniba.sk:3306";
$database_user = "cnc";
$database_pass = "c0gsc1";
$database_name = "cnc";
*/
$database_host = "127.0.0.1:3306";
$database_user = "cnc";
$database_pass = "c0gsc1";
$database_name = "cnc";

//DATA::
$mainTable = "home_cnc";
$userTable = "users";
$projTable = "projects";

$projTableFields = array("id", "projectname", "description", "leader","vis");
$projTableString = "id, projectname, description, leader, vis";

$publiTable = "publications";
$projxpub = "projectxpublication";
$userxproj = "userxproject";
$userxpub = "userxpublication";

$entrylimit = 20;
$publiTableFields = array("id", "vis", "ptype", "name", "address", "author", "booktitle", "edition", "editor", "institution", "journal", 
"month", "note", "number", "organization", "pages", "publisher", "school", "series", "title", "volume", "year", "url");

$publiTableString = "id, vis, ptype, name, address, author, booktitle, edition, editor, institution, journal, month, note, number, organization, pages, publisher, school, series, title, volume, year, url";

$bibfieldsSorted = array('author', 'title', 'year', 'pages', 'month', 'note',  'journal', 'volume', 'number', 'editor', 'publisher', 
'series', 'address', 'edition', 'booktitle', 'organization', 'school', 'institution');

$bibtypes = array("article", "book", "conference", "inbook", "incollection", "inproceedings", "techreport", "mastersthesis", "misc", "phdthesis", "phdthesis", "unpublished");

//GENERIC TEXTS
$errormessage = "<br/><p>An error ocurred during page generation.</p>"."\n";
$errorlogin = "Wrong username / password."."\n";
$errordatabase = '<p class="warning">Unable to establish connection with the database</p>'."\n";
$errordbupload = "Database error";
?>
