<?php
	$slovakDays = array(
		"Monday" => "Pondelok",
		"Tuesday" => "Utorok",
		"Wednesday" => "Streda",
		"Thrusday" => "Štvrtok",
		"Friday" => "Piatok",
		"Sathurday" => "Sobota",
		"Sunday" => "Nedeľa",		
	);
	$semstart = (date('m') < 8) ? mktime(0, 0, 0, 1, 1, date("Y")) : mktime(0, 0, 0, 8, 1, date("Y"));
    $pervsemstart = (date('m') < 8) ? mktime(0, 0, 0, 8, 1, date("Y")-1) : mktime(0, 0, 0, 1, 1, date("Y"));
	$sem = (date('m') < 8) ? "letný semester ".(date("Y")-1)."/".(date("Y")) : "zimný semester ".date("Y")."/".(date("Y")+1);
    $prevsem = (date('m') < 8) ? "zimný semester ".(date("Y")-1)."/".date("Y") : "letný semester ".(date("Y")-1)."/".(date("Y"));
?>

<p>Čas a miesto konania seminára v letnom semestri: <strong>15-16:20 v miestnosti I-9 (pavilón informatiky) na FMFI UK<!--BC35 na FIIT STU--></strong></p>

<h2>Program na <?= $sem ?></h2>
<?php	
	$query = "SELECT * FROM aiseminar WHERE datetime > '".date("Y-m-d",$semstart)."' ORDER BY datetime ASC";
	$res = mysql_query($query);
	if ($res) {
		$row = array();
		$counter = 1;
		while ($row = mysql_fetch_array($res)) { 
?>
<hr/>
<h3>
	<?=date("d.m.Y",strtotime($row['datetime']))?> - 
	<?= ($row['url'])?"<a href=".$row['url'].">":"";?><strong><?= $row['lecturer'] ?></strong><?= ($row['url'])?"</a>":"";?>
	 (<?= $row['lecturerfrom'] ?>)
</h3>
<div class="clear"></div>
<?php	if ($row['note']) {				?>
<p class="note"><?= $row['note'] ?></p>
<?php } ?>
<p>
	<?= (!($row['abstract']))?": <i>".$row['title']."</i>":"";?>
</p>
<?php if ($row['abstract']) { ?>				
<h4><?= $row['title'] ?></h4>
<p><?= $row['abstract'] ?></p>
<?php
		}
	} 
}?>

<?php
	$query = "SELECT * FROM aiseminar WHERE datetime > '".date("Y-m-d",$pervsemstart)."' AND datetime < '".date("Y-m-d",$semstart)."' ORDER BY datetime ASC";
	$res = mysql_query($query);
	if ($res && mysql_num_rows($res) > 0) {
?>
        <h2>Program na <?= $prevsem ?></h2>
    <?php
		$row = array();
		$counter = 1;
		while ($row = mysql_fetch_array($res)) { 
?>
<hr/>
<h3>
	<?=date("d.m.Y",strtotime($row['datetime']))?> - 
	<?= ($row['url'])?"<a href=".$row['url'].">":"";?><strong><?= $row['lecturer'] ?></strong><?= ($row['url'])?"</a>":"";?>
	 (<?= $row['lecturerfrom'] ?>)
</h3>
<div class="clear"></div>
<?php	if ($row['note']) {				?>
<p class="note"><?= $row['note'] ?></p>
<?php } ?>
<p>
	<?= (!($row['abstract']))?": <i>".$row['title']."</i>":"";?>
</p>
<?php if ($row['abstract']) { ?>				
<h4><?= $row['title'] ?></h4>
<p><?= $row['abstract'] ?></p>
<?php
		}
	} 
}?>
