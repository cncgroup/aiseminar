<h2>Vyhľadávanie</h2>

<form name="search" method="POST">
	<fieldset>
		<label for="searchyear">rok: </label>
		<?php echo allyearsSelect("searchyear");?>
		<div class="clear"></div>
		<label for="searchlecturer">prednášajúci/a: </label>
		<input type="text" name="searchlecturer" value="<?php echo (isset($_POST['searchlecturer']) && $_POST['searchlecturer'] != "") ? $_POST['searchlecturer'] : ""?>"/>
		<div class="clear"></div>
		<label for="searchkeyword">kľúčové slovo: </label>
		<input type="text" name="searchkeyword" value="<?php echo (isset($_POST['searchkeyword']) && $_POST['searchkeyword'] != "") ? $_POST['searchkeyword'] : ""?>"/>	
		<input type="submit" value="Hľadať" class="right"/>
	</fieldset>
</form>

<?php	
	if (isset($_POST) && (
			isset($_POST['searchyear']) && $_POST['searchyear'] != "všetky" ||
			isset($_POST['searchlecturer']) && $_POST['searchlecturer'] != "" || 
			isset($_POST['searchkeyword']) && $_POST['searchkeyword'] != "")) 
	{
?>		
<h2>Výsledky vyhľadávania</h2>
<?php		
		$query = "SELECT * FROM aiseminar WHERE";
		
		if (isset($_POST['searchyear']) && $_POST['searchyear'] != "všetky")
			$query .= " YEAR(datetime) = '".$_POST['searchyear']."' AND "; 
		
		if (isset($_POST['searchlecturer']) && $_POST['searchlecturer'] != "")
			$query .= " lecturer LIKE  '%".$_POST['searchlecturer']."%' AND "; 
		
		if (isset($_POST['searchkeyword']) && $_POST['searchkeyword'] != "")
			$query .= " title LIKE '%".$_POST['searchkeyword']."%' || abstract LIKE '%".$_POST['searchkeyword']."%' AND "; 
		
		if (substr($query,strlen($query)-5,strlen($query)) == " AND ")
			$query = substr($query,0,strlen($query)-5);
		
		if (substr($query,strlen($query)-6,strlen($query)) == " WHERE")
			$query = substr($query,0,strlen($query)-6);
		
		$query .= " ORDER BY datetime DESC";
		
		$res = mysql_query($query);
		if ($res) {
			$row = array();
			while ($row = mysql_fetch_array($res)) { 
			?>
				<hr/>
				<h3>
					<?=date("d.m.Y",strtotime($row['datetime']))?> - 
					<?= ($row['url'])?"<a href=".$row['url'].">":"";?><strong><?= $row['lecturer'] ?></strong><?= ($row['url'])?"</a>":"";?>
					(<?= $row['lecturerfrom'] ?>)
				</h3>
				<div class="clear"></div>
				<p>
					<?= (!($row['abstract']))?": <i>".$row['title']."</i>":"";?>
				</p>
				<?php if ($row['abstract']) { ?>				
				<h4><?= $row['title'] ?></h4>
				<p><?= $row['abstract'] ?></p>
				<?php
				}
			}
		}
	}
?>
