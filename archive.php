<h2>Archív seminárov</h2>
<ul>
<?php
	$qyrs = "SELECT MAX(datetime) as max, MIN(datetime) as min FROM aiseminar";
	$ryrs = mysql_query($qyrs);
	if ($ryrs) {
		$min = substr(mysql_result($ryrs, 0, 'min'),0,4);
		$max = substr(mysql_result($ryrs, 0, 'max'),0,4);
		$minm = substr(mysql_result($ryrs, 0, 'min'),5,2);
		$c = ($minm > 8)?$min:$min-1;
		while ($c < date('Y')) {
			$yrs = $c."/".($c+1);
?>
			<li><a href="?action=archive&amp;year=<?=$c."/".($c+1)?>">Školský rok <?=$c."/".($c+1)?></a></li>
<?php			
			$c++;
		}
		
		mysql_free_result($ryrs);
	}
?>
</ul>
<?php
	$slovakDays = array(
		"Monday" => "Pondelok",
		"Tuesday" => "Utorok",
		"Wednesday" => "Streda",
		"Thrusday" => "Štvrtok",
		"Friday" => "Piatok",
		"Sathurday" => "Sobota",
		"Sunday" => "Nedeľa",		
	);
	
	if (isset($_GET['action']) && $_GET['action'] == "archive" && isset($_GET['year'])) {
		
		$years = explode("/",$_GET['year']);
		
		foreach ($years as $i => $y) {
			if ($i == 0) {
				//zimny semester
				$semstart = mktime(0, 0, 0, 8, 1, $years[0]);
				$semend = mktime(0, 0, 0, 1, 15, $years[1]);
				$sem = "zimný semester ".$years[0]."/".$years[1];
			} else {
				//letny semester
				$semstart = mktime(0, 0, 0, 1, 15, $years[1]);
				$semend = mktime(0, 0, 0, 8, 1, $years[1]);
				$sem = "letný semester ".$years[0]."/".$years[1];
			}
			$query = "SELECT * FROM aiseminar WHERE datetime > '".date("Y-m-d",$semstart)."' AND datetime < '".date("Y-m-d",$semend)."' ORDER BY datetime ASC";
			$res = mysql_query($query);
			if ($res) {
				if (mysql_num_rows($res) > 0) {
				?><h2>Program na <?= $sem ?></h2><?php
				}	
				$row = array();
				$datetime = "";
				$counter = 1;
				while ($row = mysql_fetch_array($res)) { 
					if ($row['datetime'] != $datetime) {
		?>
						<h3><?=$counter?>. seminár</h3>
						<p class="date">
							(<?= $slovakDays[date("l",strtotime($row['datetime']))]." ".date("d.m.Y",strtotime($row['datetime']))?>
							 o <?= date("G:i",strtotime($row['datetime']))?>)
						</p>
						<div class="clear"></div>
		<?php			if ($row['note']) {				?>
							<p class="note"><?= $row['note'] ?></p>
		<?php				
						}
						$datetime = $row['datetime'];
					}
		?>
					<p>
						<strong><?= $row['lecturer'] ?></strong> 
						<small>(<?= $row['lecturerfrom'] ?>)</small>
						<?= ($row['url'])?", <a href=".$row['url'].">domovská stránka</a>":"";?>
						<?= (!($row['abstract']))?": <i>".$row['title']."</i>":"";?>
					</p>
		<?php		if ($row['abstract']) { ?>				
					<h4><?= $row['title'] ?></h4>
					<p><?= $row['abstract'] ?></p>
		<?php
					}
					$counter++;
				} 
				mysql_free_result($res);
			} else
				echo "Q. failed: ".$query."<br/>";
		}
	}
?>
